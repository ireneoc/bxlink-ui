import components from './components'

export const install = Vue => {
  for (const prop in components) {
    if (components.hasOwnProperty(prop)) {
      const component = components[prop]
      Vue.component(component.name, component)
    }
  }
}

const bxui = { install }
export default bxui
